var TableView = function (elem, onNeedRowIDs, onNeedRowForRowID, columns) {
    this.onReorder = false;
    this.onCanEdit = false;

    try {
        var tmp = (elem.substr(0,1).length!=1);
        elem = document.getElementById(elem);
    } catch (err) {}
    this._rootElem = elem;
    this._headerElem = false;
    this._contentElem = false;
    this._contentSimulationElem = false;
    this._filterElems = {};
    this._dragElem = false;
    this._dropMarkerElem = false;

    this._colWids = {}; // colInd => colWid
    this._filters = {};
    this._rowIDs = []; // rowInd => rowID
    this._rRowIDs = {}; // rowID => rowInd
    this._rows = {}; // rowID => row
    this._columns = []; // colInd => col
    this._rColumns = {}; // col => colInd
    this._cells = {}; // rowIndModulo, colInd => cell
    this._rowIndices = {}; // rowIndModulo => rowInd
    this._touches = {};
    this._scrollTimer = false;
    this._drawTimer = false;
    this._scrollSequenceNum = 0;
    this._style = {
        scrollBarWidth:-1,
        scrollBarHeight:-1,
        headerHeight:20,
        headerBarStyle:"background-color:rgb(220,220,220);",
        headerCellStyle:"color:rgb(0,0,0);",
        headerRightStyle:"background-color:rgb(210,210,210);",
        rowHeight:20,
        contentFontSize:14,
        contentStyle:"-webkit-overflow-scrolling:touch; background-color:rgb(245,245,245);",
        contentCellStyle:"",
        evenRowBGColor:"rgb(255,255,255)",
        evenRowColor:"rgb(0,0,0)",
        oddRowBGColor:"rgb(240,240,240)",
        oddRowColor:"rgb(0,0,0)",
        selectedEvenRowBGColor:"rgb(75,75,75)",
        selectedEvenRowColor:"rgb(255,255,255)",
        selectedOddRowBGColor:"rgb(60,60,60)",
        selectedOddRowColor:"rgb(255,255,255)",
    };
    this._state = {pxYOff:0, pxXOff:0, selection:{}, lastSelection:false, editing:false};
    if (columns) this.setColumns(columns);
    if (onNeedRowIDs && onNeedRowForRowID) this.setDataSource(onNeedRowIDs, onNeedRowForRowID);
    this.draw();
};
TableView.prototype.setDataSource = function (onNeedRowIDs, onNeedRowForRowID) {
    this._rowIDs = [];
    this._onNeedRowIDs = onNeedRowIDs;
    this._onNeedRowForRowID = onNeedRowForRowID;
    this.forceReload();
};
TableView.prototype.setColumns = function (newColumns) {
    this._columns = newColumns;
    this._rColumns = {};
    for (var i=0; i<newColumns.length; i++) {
        this._rColumns[newColumns[i]] = i;
    }
    this.draw();
};
TableView.prototype.setStyle = function (newStyle) {
    for (var k in newStyle) {
        this._style[k] = newStyle[k];
    }
    this.draw();
};
TableView.prototype.resetFilters = function () {
    this._filters = {};
    this.forceReload();
};

TableView.prototype.draw = function () {
    if (this._drawTimer) window.clearTimeout(this._drawTimer);
    this._drawTimer = window.setTimeout(this.doDraw.bind(this), 25);
};
TableView.prototype.doDraw = function () {
    if (this._drawTimer) window.clearTimeout(this._drawTimer);
    this._drawTimer = false;
    console.log("DO DRAW", this._rootElem.id);
    // TODO: determine scrollbarwid/hei if -1
    this._rootElem.innerHTML = "";
    this._rootElem.style.overflow = "hidden";
    var rootWid = this._rootElem.offsetWidth;
    this._headerElem = document.createElement("div");
    this._headerElem.setAttribute("style", "position:absolute; overflow:hidden; left:0px; top:0px; right:0px; width:auto; height:"+this._style.headerHeight+"px; line-height:"+this._style.headerHeight+"px; "+this._style.headerBarStyle);
    var collen = this._columns.length;
    var xoff = 0;
    for (var i=0; i<collen; i++) {
        var col = this._columns[i];
        var wid = col.getWidthFunc(rootWid-this._style.scrollBarWidth);
        this._colWids[i] = wid;
        var headerCellElem = document.createElement("div");
        headerCellElem.setAttribute("style", "position:absolute; top:0px; left:"+xoff+"px; width:"+wid+"px; height:"+this._style.headerHeight+"px; overflow:hidden; "+this._style.headerCellStyle);
        headerCellElem.innerHTML = col.title;
        if (col.filterFunc) {
            var filterSpan = document.createElement("span");
            filterSpan.setAttribute("style", "position:relative;");
            var filterCnv = document.createElement("canvas");
            var cnvsiz = this._style.headerHeight*2;
            filterCnv.setAttribute("width", cnvsiz);
            filterCnv.setAttribute("height", cnvsiz);
            filterCnv.setAttribute("style", "position:absolute; width:"+(this._style.headerHeight)+"px; height:"+(this._style.headerHeight)+"px; margin:0px;");
            var drawFilter = (function (filterCnv) {return function (active) {
                var ctx = filterCnv.getContext("2d");
                ctx.clearRect(0, 0, cnvsiz, cnvsiz);
                ctx.strokeStyle = headerCellElem.style.color;
                ctx.fillStyle = headerCellElem.style.color;
                var tmp = (48<cnvsiz)?4:cnvsiz/12;
                ctx.lineWidth = 2;
                ctx.beginPath();
                ctx.moveTo(cnvsiz*2/5, cnvsiz*1/5);
                ctx.lineTo(cnvsiz*4/5, cnvsiz*1/5);
                ctx.lineTo(cnvsiz/2+tmp, cnvsiz/2-tmp);
                ctx.lineTo(cnvsiz/2+tmp, cnvsiz*4/5-2*tmp);
                ctx.lineTo(cnvsiz/2-tmp, cnvsiz*4/5);
                ctx.lineTo(cnvsiz/2-tmp, cnvsiz/2-tmp);
                ctx.lineTo(cnvsiz*1/5, cnvsiz*1/5);
                ctx.lineTo(cnvsiz*3/5, cnvsiz*1/5);
                ctx.stroke();
                if (active) ctx.fill();
            };})(filterCnv);
            drawFilter("_col_filter_"+col.title in this._filters);
            filterSpan.appendChild(filterCnv);
            filterSpan.onclick = function (col, headerCellElem, drawFilter) {
                if ("_col_filter_"+col.title in this._filterElems) {
                    try {
                        var filterElem = this._filterElems["_col_filter_"+col.title];
                        if (this._rootElem) this._rootElem.removeChild(filterElem);
                    } catch (err) {}
                    delete this._filterElems["_col_filter_"+col.title];
                } else {
                    for (var key in this._filterElems) {
                        try {
                            var filterElem = this._filterElems[key];
                            if (this._rootElem) this._rootElem.removeChild(filterElem);
                        } catch (err) {}
                        delete this._filterElems[key];
                    }
                    var rect = headerCellElem.getBoundingClientRect();
                    var filterElem = document.createElement("div");
                    filterElem.style.width = rect.width+"px";
                    if (this._rootElem) this._rootElem.appendChild(filterElem);
                    col.filterFunc(filterElem, this._filters["_col_filter_"+col.title], function (filterElem, newDesc) {
                        if (newDesc===false) {
                            delete this._filters["_col_filter_"+col.title];
                            drawFilter(false);
                        } else {
                            this._filters["_col_filter_"+col.title] = newDesc;
                            drawFilter(true);
                        }
                        this.forceReload();
                    }.bind(this, filterElem));
                    filterElem.setAttribute("style", "position:absolute; top:"+(this._style.headerHeight)+"px; left:"+(headerCellElem.offsetLeft)+"px; z-index:10000;");
                    this._filterElems["_col_filter_"+col.title] = filterElem;
                }
            }.bind(this, col, headerCellElem, drawFilter);
            headerCellElem.appendChild(filterSpan);
        }
        this._headerElem.appendChild(headerCellElem);
        xoff += wid;
    }
    var headerRightElem = document.createElement("div");
    headerRightElem.setAttribute("style", "position:absolute; left:"+xoff+"px; top:0px; width:"+this._style.scrollBarWidth+"px; height:"+this._style.headerHeight+"px; "+this._style.headerRightStyle);
    this._headerElem.appendChild(headerRightElem);
    this._rootElem.appendChild(this._headerElem);

    this._contentElem = document.createElement("div");
    this._contentElem.setAttribute("style", "position:absolute; left:0px; top:"+this._style.headerHeight+"px; right:0px; bottom:0px; width:auto; height:auto; overflow:scroll; "+this._style.contentStyle);
    this._rootElem.appendChild(this._contentElem);
    this._contentElem.onscroll = function (e) {
        if (this._scrollTimer) window.clearTimeout(this._scrollTimer);
        this._scrollSequenceNum++;
        if (1024<this._scrollSequenceNum) this._scrollSequenceNum -= 1024;
        var localScrollSequenceNum = this._scrollSequenceNum;
        var pxXOff = this._contentElem.scrollLeft;
        var pxYOff = this._contentElem.scrollTop;
        this._headerElem.scrollLeft = pxXOff;
        this._state.pxXOff = pxXOff;
        this._state.pxYOff = pxYOff;
        this._scrollTimer = window.setTimeout(function (localScrollSequenceNum) {
            this.refresh(localScrollSequenceNum);
        }.bind(this, localScrollSequenceNum), 100);
    }.bind(this);

    this._visibleRows = Math.ceil(this._contentElem.offsetHeight/this._style.rowHeight);
    this._visibleRows = Math.floor(this._visibleRows/2+1)*2;
    this._contentSimulationElem = document.createElement("div");
    this._contentSimulationElem.setAttribute("style", "background-color:rgb(255,255,255); "+this._style.contentStyle+"; position:absolute; left:0px; top:0px; right:0px; width:auto; height:"+this._style.rowHeight+"px; -moz-user-select:none; -webkit-user-select:none; -ms-user-select:none;");
    this._contentElem.appendChild(this._contentSimulationElem);
    for (var y=0; y<this._visibleRows; y++) {
        this._cells[y] = {};
        this._rowIndices[y] = -1;
        var tmpRow = document.createElement("div");
        tmpRow.setAttribute("style", "position:absolute; left:0px; top:0px; height:"+this._style.rowHeight+"px; line-height:"+this._style.rowHeight+"px; font-size:"+Math.floor(this._style.contentFontSize)+"px;  overflow:visible; white-space:nowrap; cursor:default;");
        tmpRow.draggable = true;
        tmpRow.ondragstart = function (e) {
            var dragElem = document.createElement("div");
            dragElem.setAttribute("style", "position:absolute; top:0px; left:0px; width:1px; height:1px; overflow:hidden; cursor:default; opacity:0.5; z-index:-1;");
            var ind = 0;
            var data = [];
            for (var i=0; i<this._rowIDs.length; i++) {
                var tmpID = this._rowIDs[i];
                if (tmpID in this._state.selection) { // For all selected Rows (but in the right order!)
                    data.push(parseInt(tmpID));
                    var xoff = 0;
                    for (var x=0; x<this._columns.length; x++) {
                        var colwid = this._colWids[x];
                        var tmp = document.createElement("div");
                        tmp.setAttribute("style", "position:absolute; left:"+xoff+"px; top:"+(ind*this._style.rowHeight)+"px; width:"+colwid+"px; height:"+this._style.rowHeight+"px; line-height:"+this._style.rowHeight+"px; font-size:"+Math.floor(this._style.contentFontSize)+"px;  background-color:"+(ind%2==0?this._style.selectedEvenRowBGColor:this._style.selectedOddRowBGColor)+"; color:"+(ind%2==0?this._style.selectedEvenRowColor:this._style.selectedOddRowColor)+"; overflow:hidden; white-space:nowrap; cursor:default;");
                        dragElem.appendChild(tmp);
                        xoff += colwid;
                        this._columns[x].renderCellFunc(this, tmp, this._rows[tmpID]);
                    }
                    dragElem.style.width = xoff+"px";
                    ind++;
                }
            }
            dragElem.style.height = (ind*this._style.rowHeight)+"px";
            this._contentSimulationElem.appendChild(dragElem);
            this._dragElem = dragElem;
            e.dataTransfer.setDragImage(dragElem, 0, 0);
            e.dataTransfer.setData("application/x-buefflae-tablerow-"/* TODO */, JSON.stringify(data));
            e.dataTransfer.setData("text/plain", "text/plain"+dragElem.contentText);
            e.dataTransfer.setData("text/html", "text/html"+dragElem.innerHTML);
        }.bind(this);
        tmpRow.ondragend = function (e) {
            e.preventDefault();
            this._dragElem.parentElement.removeChild(this._dragElem);
        }.bind(this);
        tmpRow.ondragover = function (tmpRow, e) {
            if (this.onReorder) {
                var success = false;
                for (var i=0; i<e.dataTransfer.types.length; i++) {
                    if (e.dataTransfer.types[i]=="application/x-buefflae-tablerow-"/* TODO */) {
                        e.dataTransfer.dropEffect = "move";
                        e.preventDefault();
                        success = true;
                        break;
                    }
                }
                if (success) {
                    var dropMarkerElem = this._dropMarkerElem;
                    if (!dropMarkerElem) {
                        dropMarkerElem = document.createElement("div");
                        dropMarkerElem.setAttribute("style", "position:fixed; height:2px; background-color:rgb(0,0,0); z-index:1; pointer-events:none;");
                        document.body.appendChild(dropMarkerElem);
                        this._dropMarkerElem = dropMarkerElem;
                    }
                    var rowRect = tmpRow.getBoundingClientRect();
                    var contentRect = this._contentElem.getBoundingClientRect();
                    var isTop = (e.clientY-rowRect.top-rowRect.height/2<0);
                    dropMarkerElem.style.width = Math.min(contentRect.width, rowRect.width)+"px";
                    dropMarkerElem.style.top = (isTop?rowRect.top-1:rowRect.bottom-1)+"px";
                    dropMarkerElem.style.left = contentRect.left+"px";
                }
            }
        }.bind(this, tmpRow);
        tmpRow.ondragleave = function (e) {
            e.preventDefault();
            if (this.onReorder) {
                if (this._dropMarkerElem) {
                    this._dropMarkerElem.parentElement.removeChild(this._dropMarkerElem);
                    this._dropMarkerElem = false;
                }
            }
        }.bind(this);
        tmpRow.ondrop = function (tmpRow, y, e) {
            e.preventDefault();
            if (this.onReorder) {
                if (this._dropMarkerElem) {
                    this._dropMarkerElem.parentElement.removeChild(this._dropMarkerElem);
                    this._dropMarkerElem = false;
                }
                var sourceIDs = JSON.parse(e.dataTransfer.getData("application/x-buefflae-tablerow-"/* TODO */));
                var rowRect = tmpRow.getBoundingClientRect();
                var isTop = (e.clientY-rowRect.top-rowRect.height/2<0);
                var ind = this._rowIndices[y];
                var destinationIDs = [this._rowIDs[ind], this._rowIDs[ind+1]];
                if (isTop) destinationIDs = [this._rowIDs[ind-1], this._rowIDs[ind]];
                var finished = function (rowIDs) {
                    this._rowIDs = rowIDs;
                    this._rRowIDs = {};
                    for (var i=0; i<rowIDs.length; i++) {
                        this._rRowIDs[rowIDs[i]] = i;
                    }
                    this.forceRefresh();
                };
                var newIDs = [];
                if (destinationIDs[0]==undefined) newIDs = sourceIDs;
                var sourceIDsDict = {};
                for (var i=0; i<sourceIDs.length; i++) sourceIDsDict[sourceIDs[i]] = true;
                for (var i=0; i<this._rowIDs.length; i++) {
                    var tmpID = this._rowIDs[i];
                    if (!(tmpID in sourceIDsDict)) newIDs.push(tmpID);
                    if (destinationIDs[0]==tmpID) newIDs = newIDs.concat(sourceIDs);
                }
                this.onReorder(this, sourceIDs, destinationIDs, this._rowIDs, newIDs, finished);
                return false;
            }
        }.bind(this, tmpRow, y);
        var xoff = 0;
        for (var x=0; x<collen; x++) {
            var colwid = this._colWids[x];
            var tmpCell = document.createElement("div");
            tmpCell.setAttribute("style", "position:absolute; left:"+xoff+"px; top:0px; width:"+(colwid-6)+"px; height:"+this._style.rowHeight+"px; line-height:"+this._style.rowHeight+"px; padding:0px 3px; font-size:"+Math.floor(this._style.contentFontSize)+"px;  overflow:visible; white-space:nowrap; cursor:default;"+this._style.contentCellStyle);
            tmpRow.appendChild(tmpCell);
            this._cells[y][x] = tmpCell;
            xoff += colwid;
        }
        tmpRow.style.width = xoff+"px";
        this._contentSimulationElem.style.width = xoff+"px";
        this._cells[y].row = tmpRow;
        this._contentSimulationElem.appendChild(tmpRow);
    }
    this._contentElem.scrollLeft = this._state.pxXOff;
    this._contentElem.scrollTop = this._state.pxYOff;
    if (this._state.pxYOff==0) window.setTimeout(this.forceRefresh.bind(this), 1);
};

TableView.prototype.refresh = function (localScrollSequenceNum) {
    if (!this._contentElem) return;
    var collen = this._columns.length;
    var rowNum = this._rowIDs.length;
    var pxYOff = this._contentElem.scrollTop;
    var rowsYOff = Math.floor(pxYOff/this._style.rowHeight);
    var limit = rowsYOff+this._visibleRows;
    if (rowNum<limit) limit = rowNum;
    this._contentSimulationElem.style.height = (rowNum*this._style.rowHeight)+"px";
    var loadIDs = [];
    var loadYMods = [];
    var loadYs = [];
    for (var y=limit-this._visibleRows; y<limit && (localScrollSequenceNum==undefined || localScrollSequenceNum==this._scrollSequenceNum); y++) {
        var ymod = y%this._visibleRows;
        var visible = (0<=y);
        while (ymod<0) ymod += this._visibleRows;
        this._cells[ymod].row.style.top = (y*this._style.rowHeight)+"px";
        this._cells[ymod].row.style.visibility = (visible?"visible":"hidden")+"px";
        if (!visible) continue;
        var ind = this._rowIndices[ymod];
        var tmpID = this._rowIDs[y];
        if (ind!=y) {
            for (var x=0; x<collen && (localScrollSequenceNum==undefined || localScrollSequenceNum==this._scrollSequenceNum); x++) {
                var tmp = this._cells[ymod][x];
                var refreshView = function (tmpID) {
                    var collen = this._columns.length;
                    for (var j=0; j<this._visibleRows; j++) {
                        var ind = this._rowIndices[j];
                        var tmpID = this._rowIDs[ind];
                        for (var x=0; x<collen; x++) {
                            var tmp = this._cells[j][x];
                            if (tmpID in this._state.selection) {
                                tmp.style.backgroundColor = this._style[((j%2)==0?"selectedEvenRowBGColor":"selectedOddRowBGColor")];
                                tmp.style.color = this._style[((j%2)==0?"selectedEvenRowColor":"selectedOddRowColor")];
                            } else {
                                tmp.style.backgroundColor = this._style[((j%2)==0?"evenRowBGColor":"oddRowBGColor")];
                                tmp.style.color = this._style[((j%2)==0?"evenRowColor":"oddRowColor")];
                            }
                        }
                    }
                }.bind(this, tmpID);
                /*
                tmp.ontouchstart = function (e) {
                    document.body.style.backgroundColor = "blue";
                    document.getElementById("status").innerHTML = (e.changedTouches[0].identifier);
                };
                */
                tmp.onmousedown = function (tmpID, tmpCol, refreshView, e) { // TODO: touch?
                    this._touches["mouse"] = {"ox":e.clientX, "oy":e.clientY, "x":e.clientX, "y":e.clientY, "oid":tmpID};
                    if (e.metaKey) {
                        if (!(tmpID in this._state.selection)) {
                            this._state.lastSelection = tmpID;
                            this._state.selection[tmpID] = {col:tmpCol, time:Date.now()};
                            refreshView();
                            this._touches["mouse"].metaSelected = true;
                        }
                    } else if (e.shiftKey && this._state.lastSelection!==false) {
                        var active = 0;
                        for (var i=0; i<this._rowIDs.length; i++) {
                            if (!active) {
                                if (this._rowIDs[i]==this._state.lastSelection) active = 1;
                                if (this._rowIDs[i]==tmpID) active = 2;
                            }
                            if (0<active) {
                                this._state.selection[this._rowIDs[i]] = {col:tmpCol, time:Date.now()};
                                if (this._rowIDs[i]==this._state.lastSelection && active==2) break;
                                if (this._rowIDs[i]==tmpID && active==1) break;
                            }
                        }
                        refreshView();
                    } else {
                        if (!(tmpID in this._state.selection)) {
                            var tmp = this._state.selection[tmpID];
                            this._state.selection = {};
                            this._state.lastSelection = tmpID;
                            this._state.selection[tmpID] = {col:tmpCol, time:(tmp?tmp.time:Date.now())};
                            refreshView();
                        }
                    }
                }.bind(this, tmpID, this._columns[x], refreshView);
                tmp.onmouseup = function (tmpID, tmpCol, refreshView, e) { // TODO: touch?
                    if (e.metaKey) {
                        if (tmpID in this._state.selection && !this._touches["mouse"].metaSelected) {
                            delete this._state.selection[tmpID];
                            refreshView();
                        }
                    } else if (e.shiftKey && this._state.lastSelection!==false) {
                    } else {
                        var tmp = this._state.selection[tmpID];
                        this._state.selection = {};
                        this._state.lastSelection = tmpID;
                        this._state.selection[tmpID] = {col:tmpCol, time:(tmp?tmp.time:Date.now())};
                        refreshView();
                    }
                    delete this._touches["mouse"];
                    e.preventDefault();
                }.bind(this, tmpID, this._columns[x], refreshView);
                tmp.onclick = function (tmpID, tmpCol, tmp, e) {
                    var msSinceSelection = Date.now()-this._state.selection[tmpID].time;
                    console.log("Click", tmpID, tmpCol, msSinceSelection);
                    if (600<msSinceSelection && (this._state.editing[0]!=tmpCol || this._state.editing[1]!=tmpID)) {
                        var res = false;
                        if (tmpCol.editCellFunc) res = true;
                        if (this.onCanEdit) res = this.onCanEdit(this, tmpID, tmpCol);
                        if (res) {
                            if (tmpCol.editCellFunc) {
                                tmpCol.editCellFunc(this, tmp, tmpID, tmpCol);
                                this._state.editing = [tmpCol, tmpID];
                            }
                        }
                    }
                }.bind(this, tmpID, this._columns[x], tmp);
                tmp.ondblclick = function (tmpID, tmpCol, e) {
                    var msSinceSelection = Date.now()-this._state.selection[tmpID].time;
                    console.log("DblClick", tmpID, tmpCol, msSinceSelection);
                }.bind(this, tmpID, this._columns[x]);
                if (tmpID in this._state.selection) {
                    tmp.style.backgroundColor = this._style[((y%2)==0?"selectedEvenRowBGColor":"selectedOddRowBGColor")];
                    tmp.style.color = this._style[((y%2)==0?"selectedEvenRowColor":"selectedOddRowColor")];
                } else {
                    tmp.style.backgroundColor = this._style[((y%2)==0?"evenRowBGColor":"oddRowBGColor")];
                    tmp.style.color = this._style[((y%2)==0?"evenRowColor":"oddRowColor")];
                }
                if (tmpID in this._rows) {
                    this._columns[x].renderCellFunc(this, tmp, this._rows[tmpID], this._columns[x]);
                } else {
                    tmp.innerHTML = "";
                }
            }
            this._rowIndices[ymod] = y;
            if (!(tmpID in this._rows)) {
                loadIDs.push(tmpID);
                loadYMods.push(ymod);
                loadYs.push(y);
            }
        }
    }
    if (0<loadIDs.length) {
        this._onNeedRowForRowID(this, loadIDs, function (loadIDs, loadYMods, loadYs, rows) {
            var beg = new Date().getTime();
            for (var j=0; j<loadIDs.length; j++) {
                var id = loadIDs[j];
                var ymod = loadYMods[j];
                var y = loadYs[j];
                var row = rows[j];
                this._rows[id] = row;
                if (this._rowIndices[ymod]==y) {
                    var collen = this._columns.length;
                    for (var x=0; x<collen; x++) {
                        var tmp = this._cells[ymod][x];
                        this._columns[x].renderCellFunc(this, tmp, row, this._columns[x]);
                    }
                }
            }
        }.bind(this, loadIDs, loadYMods, loadYs));
    }
};
TableView.prototype.forceRefresh = function (localScrollSequenceNum) {
    // Consider changes in selection, ids, scroll
    this._rowIndices = {};
    this.refresh(localScrollSequenceNum);
};
TableView.prototype.forceReload = function (localScrollSequenceNum) {
    // Consider changes in data of rows
    this._rows = {};
    this._onNeedRowIDs(this, this._filters, function (rowIDs) {
        this._rowIDs = rowIDs;
        this._rRowIDs = {};
        for (var i=0; i<rowIDs.length; i++) {
            this._rRowIDs[rowIDs[i]] = i;
        }
        this.forceRefresh(localScrollSequenceNum);
    }.bind(this));
};

var TableViewColumn = function (title, getWidthFunc, renderCellFunc, editCellFunc, filterFunc) {
    this.title = title;
    this.getWidthFunc = getWidthFunc;
    if (!this.getWidthFunc) this.getWidthFunc = function (wid) {
        return 200;
    };
    this.renderCellFunc = renderCellFunc;
    this.editCellFunc = editCellFunc;
    this.filterFunc = filterFunc;
};
var TableViewCellRenderText = function (gettext) {
    return function (tbl, cell, row, col) {
        try {
            cell.innerHTML = gettext(row);
        } catch (err) {}
    }
};
var TableViewCellRender = function (dorender) {
    return function (tbl, cell, row, col) {
        try {
            dorender(row, cell);
        } catch (err) {}
    }
};
var TableViewCellRenderHTMLJS = function (gethtmljs) {
    return function (tbl, cell, row, col) {
        try {
            var dict = gethtmljs(row);
            cell.innerHTML = dict["html"];
            dict["js"]();
        } catch (err) {}
    }
};

var TableViewCellEditText = function (getText, setText, onPropose, onProposalAccepted) {
    return function (tbl, cell, rowID, col) {
        try {
            cell.innerHTML = "";
            var select = document.createElement("select");
            cell.appendChild(select);
            select.setAttribute("size", "10");
            select.setAttribute("style", "position:absolute; top:"+(tbl._style.rowHeight)+"px; left:0px; z-index:1000; display:none;");

            var input = document.createElement("input");
            cell.appendChild(input);
            input.type = "text";
            input.value = getText(tbl._rows[rowID]);
            input.setAttribute("style", "position:absolute; top:0px; right:0px; bottom:0px; left:0px; width:100%; height:100%; border-width:0px; line-height:"+tbl._style.rowHeight+"px; margin:0px; padding:0px 3px; box-sizing:border-box; font-size:"+Math.floor(tbl._style.contentFontSize)+"px; z-index:999;");

            var blurWithText = function (text) {
                input.onblur = function () {};
                select.onblur = function () {};
                setText(rowID, text);
                col.renderCellFunc(tbl, cell, tbl._rows[rowID], col);
                tbl._state.editing = [];
            }
            var blurWithProposal = function (proposal) {
                input.onblur = function () {};
                select.onblur = function () {};
                onProposalAccepted(rowID, proposal);
                col.renderCellFunc(tbl, cell, tbl._rows[rowID], col);
                tbl._state.editing = [];
            }

            input.onkeydown = function (e) {
                if (e.keyCode==9) {
                    blurWithText(input.value);
                    window.setTimeout(function () {
                        var found = false;
                        for (var i=0; i<tbl._columns.length; i++) {
                            if (found && tbl._columns[i].editCellFunc) {
                                tbl._columns[i].editCellFunc(tbl, tbl._cells[tbl._rRowIDs[rowID]%tbl._visibleRows][i], rowID, tbl._columns[i]);
                                tbl._state.editing = [tbl._columns[i], rowID];
                                break;
                            }
                            if (tbl._columns[i]==col) found = true;
                        }
                    }, 25);
                } else if (e.keyCode==13) {
                    blurWithText(input.value);
                } else if (e.keyCode==40 && select.style.display=="block") {
                    select.focus();
                    select.selectedIndex = 0;
                }
            };
            input.onkeyup = function (e) {
                if (onPropose && onProposalAccepted) {
                    select.style.display = (input.value.length==0?"none":"block");
                    onPropose(tbl._rows[rowID], input.value, function (res) {
                        if (res) {
                            select.innerHTML = "";
                            for (var i=0; i<res.length; i++) {
                                var option = document.createElement("option");
                                option.value = JSON.stringify(res[i]);
                                option.innerHTML = (res[i] instanceof Array?res[i][0]:JSON.stringify(res[i]));
                                option.onclick = function (option, entry, e) {
                                    window.setTimeout(function () {
                                        blurWithProposal(entry);
                                    }, 25);
                                }.bind(window, option, res[i]);
                                select.appendChild(option);
                            }
                        }
                    });
                }
            };
            input.onblur = function (e) {
                window.setTimeout(function () {
                    if (document.activeElement != select) {
                        blurWithText(input.value);
                    }
                }, 25);
            };
            select.onkeyup = function (e) {
                if (e.keyCode==13) {
                    blurWithProposal(JSON.parse(select.options[select.selectedIndex].value));
                }
            };
            select.onblur = function (e) {
                window.setTimeout(function () {
                    if (document.activeElement != input) {
                        if (select.selectedIndex==-1) {
                            input.onblur(false);
                        } else {
                            blurWithProposal(JSON.parse(select.options[select.selectedIndex].value));
                        }
                    }
                }, 25);
            };
            input.focus();
        } catch (err) {}
    }
};
